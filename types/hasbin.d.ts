export = hasbin;
declare function hasbin(bin: any, done: any): void;
declare namespace hasbin {
  function all(bins: any, done: any): void;
  namespace all {
    function sync(bins: any): any;
  }
  function any(bins: any, done: any): void;
  namespace any {
    function sync(bins: any): any;
  }
  // Circular reference from hasbin
  const async: any;
  function every(bins: any, done: any): void;
  namespace every {
    function sync(bins: any): any;
  }
  function first(bins: any, done: any): void;
  namespace first {
    function sync(bins: any): any;
  }
  function some(bins: any, done: any): void;
  namespace some {
    function sync(bins: any): any;
  }
  function sync(bin: any): any;
}
