import * as vscode from 'vscode';
import * as cp from 'child_process';
import * as path from 'path';
import * as os from 'os';
import * as fs from 'fs';

import { Constants } from '../constants';
import { FeatureLintData } from '../FeatureLintCli/FeatureLintData';
import { FeatureLintResponse } from '../FeatureLintCli/FeatureLintResponse';
import { debounce } from 'lodash';

export default class GherkinLinter implements vscode.CodeActionProvider 
{
  constructor(private context: vscode.ExtensionContext)
  { 
    const fullDecoration: vscode.DecorationRenderOptions = {
      dark: {
        gutterIconPath: this.context.asAbsolutePath(`./app_images/gutter-icon-dark.svg`)
      },
      light: {
        gutterIconPath: this.context.asAbsolutePath(`./app_images/gutter-icon-light.svg`)
      }
    };
    this.fullCoverageDecorationType = vscode.window.createTextEditorDecorationType(fullDecoration);
    this.outputChannel = vscode.window.createOutputChannel("UnderTest.FeatureLint");
  }

  private static commandId: string = 'gherkin.runCodeAction';
  private command!: vscode.Disposable;
  private diagnosticCollection!: vscode.DiagnosticCollection;
  private fullCoverageDecorationType: vscode.TextEditorDecorationType;
  private outputChannel: vscode.OutputChannel;

  public provideCodeActions(document: vscode.TextDocument, 
                            range: vscode.Range, 
                            context: vscode.CodeActionContext, 
                            token: vscode.CancellationToken): vscode.Command[] 
  {
    // for later when when have suggestions
    /*let diagnostic:vscode.Diagnostic = context.diagnostics[0];
		return [{
			title: "Accept featurelint suggestion",
			command: GherkinLinter.commandId,
			arguments: [document, diagnostic.range, diagnostic.message]
    }];*/
    return [];
	}

  public activate(subscriptions: vscode.Disposable[]) 
  {    
    this.outputChannel.appendLine(`Activating`);
		this.command = vscode.commands.registerCommand(GherkinLinter.commandId, this.runCodeAction, this);
		subscriptions.push(this);
		this.diagnosticCollection = vscode.languages.createDiagnosticCollection();

    vscode.workspace.onDidOpenTextDocument(this.updateDiagnosticsFromFeatureLint, this, subscriptions);    
    vscode.workspace.onDidSaveTextDocument(this.updateDiagnosticsFromFeatureLint, this);

    const executeDebounceLinting = debounce(this.updateDiagnosticsFromUnsavedFile, 200);
    vscode.workspace.onDidChangeTextDocument(executeDebounceLinting, this, subscriptions);

		vscode.workspace.onDidCloseTextDocument((textDocument) => {
			this.diagnosticCollection.delete(textDocument.uri);
		}, null, subscriptions);

    // lint all open gherkin documents
    if (vscode.window.activeTextEditor)
    {
      vscode.workspace.textDocuments.forEach(this.updateDiagnosticsFromFeatureLint, this);
    }
	}
  
  public dispose(): void 
  {
		this.diagnosticCollection.clear();
		this.diagnosticCollection.dispose();
		this.command.dispose();
	}

  private updateDiagnosticsFromUnsavedFile(evt: vscode.TextDocumentChangeEvent) {
    var doc = evt.document;
    console.log(doc);

    if (evt.contentChanges.length === 0)
    {
      return;
    }

    const tmpDir = path.join(os.tmpdir(), 'undertest_featurelint');
    fs.mkdir(tmpDir, () =>  {
      const tmpFile = path.join(tmpDir, 'live.feature');
      return fs.writeFile(tmpFile, doc.getText(), () => {
        this.lintFile(doc, tmpFile);
      });
    });
  }

  private updateDiagnosticsFromFeatureLint(textDocument: vscode.TextDocument) 
  {
    this.lintFile(textDocument, textDocument.fileName);
  }

  lintFile(textDocument: vscode.TextDocument, fileName: string) 
  {
    if (textDocument.languageId !== Constants.TargetLanguageId) 
    {
			return;
		}
    
    this.outputChannel.appendLine(`Starting lint update`);
		let decoded = '';
		const diagnostics: vscode.Diagnostic[] = [];
		let options = vscode.workspace.rootPath ? { cwd: vscode.workspace.rootPath } : undefined;
		let args =  [fileName,'--json'];
		
		let childProcess = cp.spawn('featurelint', args, options);
		childProcess.on('error', (error: Error) => {
      // ok - featurelint is installed - output the returned error message
      vscode.window.showInformationMessage(`Cannot featurelint the gherkin file with error: ${error.message}`);
    });
     
    if (childProcess.pid) 
    {
			childProcess.stdout.on('data', (data: Buffer) => {
        decoded += data;
      });
      
			childProcess.stdout.on('end', () => {
        const result: FeatureLintResponse = JSON.parse(decoded);
        if (result.Successful || result.Results.length === 0)
        {
          this.outputChannel.appendLine(`Result: success`);
          this.displaySuccessResult(textDocument);
          this.diagnosticCollection.set(textDocument.uri, []);	  
          return;
        }
        
        this.outputChannel.appendLine(`Result: failure`);
        vscode.window.visibleTextEditors.forEach((textEditor) => {
          textEditor.setDecorations(this.fullCoverageDecorationType, []);
        });
            
        result.Results[0].Findings.forEach((item: FeatureLintData) => {
					let severity = item.Type === 2 ? vscode.DiagnosticSeverity.Warning : vscode.DiagnosticSeverity.Error;
					let message = item.Message;
					let range = new vscode.Range(Math.max(item.Line - 1, 0), item.Column, Math.max(item.Line - 1, 0), item.Column);
          let diagnostic = new vscode.Diagnostic(range, message, severity);          
          diagnostics.push(diagnostic);

          this.logDiagnostic(diagnostic);					
        });
        
				this.diagnosticCollection.set(textDocument.uri, diagnostics);	
			});
		}
  }

  logDiagnostic(diagnostic: vscode.Diagnostic) {
    const level = diagnostic.severity === vscode.DiagnosticSeverity.Error ? `Error` : `Warning`;
    const line = `  ${level} - ${diagnostic.range.start.line}:${diagnostic.range.start.character} - ${diagnostic.message}`;
    this.outputChannel.appendLine(line);
  }
  
  private displaySuccessResult(textDocument: vscode.TextDocument) {
    const ranges: vscode.Range[] = [];
    for(let line = 0; line < textDocument.lineCount; line++)
    {
      ranges.push(new vscode.Range(line, 0, line, 0));
    }
    
    vscode.window.visibleTextEditors.forEach((textEditor) => {
      textEditor.setDecorations(
        this.fullCoverageDecorationType,
        ranges
      );
    });
  }
	
  private runCodeAction(document: vscode.TextDocument, range: vscode.Range, message:string): any 
  {
		/*let fromRegex:RegExp = /.*Replace:(.*)==>.* /g
		let fromMatch:RegExpExecArray = fromRegex.exec(message.replace(/\s/g, ''));
		let from = fromMatch[1];
		let to:string = document.getText(range).replace(/\s/g, '')
    if (from === to) 
    {
			let newText = /.*==>\s(.*)/g.exec(message)[1]
			let edit = new vscode.WorkspaceEdit();
			edit.replace(document.uri, range, newText);
			return vscode.workspace.applyEdit(edit);
    } 
    else 
    {
			vscode.window.showErrorMessage("The suggestion was not applied because it is out of date. You might have tried to apply the same edit twice.");
    }
    */
  }
}